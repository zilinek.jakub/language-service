from flask import render_template, Blueprint, redirect, request, abort
from flask_restful import Resource
from src.lemmatization import lemmatizate_text



view = Blueprint('view', __name__)
docs: str = ""
        
# flask default routes
@view.route("/", methods=["GET"])
def index():
    return render_template("readme.html", html_docs=docs) # render md to html and serve it

#restful representation of view via class
class Lematization(Resource):
    """
        This class is representing view for incoming requests on Language Service
    """
    def check_json(self, json: dict):
        """
            Check if json matches the following format
                {
                    "data": list[str],
                    "language": str
                }
        Args:
            json (dict): json to be checked
        """
        if not bool(json) and\
                    isinstance(json.get("data"), list) and\
                    all(isinstance(elem, str) for elem in json["data"]):
            abort(400, "data doesnt contain type of list[str]")
        if not isinstance(json.get("language"), str):
            abort(400, 'json doesnt contain language or is not str, try "language": "en"')


    def post(self):
        if request.content_type == "application/json" and request.is_json:
            in_json = request.get_json() # if data is not valid this will send response with error
            self.check_json(in_json)
            return lemmatizate_text(in_json["data"], in_json["language"]), 200 # todo lematizace


    def get(self):
        return redirect("/", code=302)
