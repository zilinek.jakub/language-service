from flask import current_app
import markdown
import src.views as views
from logging.config import dictConfig
from .lemmatization import try_download

dictConfig({
    'version': 1,
    'formatters': {'default': {
        'format': '[%(asctime)s] %(levelname)s: %(message)s',
    }},
    'handlers': {'wsgi': {
        'class': 'logging.StreamHandler',
        'stream': 'ext://flask.logging.wsgi_errors_stream',
        'formatter': 'default'
    }},
    'root': {
        'level': 'INFO',
        'handlers': ['wsgi']
    }
})


def create_docs():
    current_app.logger.info("Converting README.md")
    try:
        with open('README.md', 'r') as f:
            text = f.read()
            html = markdown.markdown(text.replace("```json", "```"))
        views.docs = html
    except Exception as e:
        current_app.logger.error(f"Converting README.md failed with error:\n{e.__str__()}")

def init_lemmatization():
    try_download('cs')
    try_download('en')

def fix_ssl():
    import ssl
    # current_app.logger.info("Set default https context to unverified...")
    # ssl._create_default_https_context = ssl._create_unverified_context