import spacy_udpipe, os, sys
from flask import current_app, abort, has_request_context
import spacy_udpipe
from flask import current_app, abort
installed = {}
"""
    represents installed languages
    
    Format:
        'cs' : <loaded spacy_udpipe class for preprocessing> 
                -> Loading class right after downloading it saves 3s for each response
"""

def try_download(language: str):
    """Check if language is installed (not in installed dict)
            - if language is already installed does nothing
            - download and LOAD the Language to dict - installed

    Args:
        language (str): language to install
    """
    os.environ["HTTP_PROXY"] = "http://fwdc2.cpas.cz:3128"
    os.environ["HTTPS_PROXY"] = "http://fwdc2.cpas.cz:3128"
    # os.environ["REQUESTS_CA_BUNDLE"]="/usr/local/lib/python3.9/site-packages/certifi/cacert.pem"
    if language not in installed:
        try:
            current_app.logger.info(f"Downloading and loading pretrained model for {language}")
            spacy_udpipe.download(language)
            installed[language] = spacy_udpipe.load(language)
        except Exception as e:
            if has_request_context():
                abort(400,f"spacy_udpipe failed to download language: {language} with error message:\n{e.__str__()}")
            else:
                current_app.logger.error(f"Application will not run as expected, restart the application. Couldn't download language with error message: \n\n\t\t{e}")
                sys.exit()
        current_app.logger.info(f"Successfully installed {language}")
        current_app.logger.info(installed.keys())

def lemmatizate_text(text: list[str], language: str = "cs"):
    """Lematizates text using ud_pipe AI, ud_pipe is wrapped with spacy library - more info here https://pypi.org/project/spacy-udpipe/

    Args:
        text (list[str]): list of strings, each will be processed independently.
        language (str, optional): Language that specifies the processing of sentences. Defaults to "cs".
        
    Returns:
        list[str]: lematized text
    """
    try_download(language)
    npl = installed[language]

    return_list: list[str] = []

    for sentence in text:
        res = ""
        if not text:
            continue
        doc = npl(sentence)
        for token in doc:
            res += token.lemma_ + " "
        return_list.append(res.strip())
    return return_list