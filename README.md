- [1. Language Services aka LaSer](#1-language-services-aka-laser)
  - [1.1. Instalation](#11-instalation)
    - [1.1.1. Docker](#111-docker)
      - [1.1.1.1. Build docker image](#1111-build-docker-image)
      - [1.1.1.2. Create running container](#1112-create-running-container)
    - [1.1.2. Manual setup](#112-manual-setup)
      - [1.1.2.1. Install python](#1121-install-python)
      - [1.1.2.2. Install venv module](#1122-install-venv-module)
      - [1.1.2.3. Create virtual enviroment](#1123-create-virtual-enviroment)
      - [1.1.2.4. Create virtual enviroment](#1124-create-virtual-enviroment)
      - [1.1.2.5. Install install requirements.txt](#1125-install-install-requirementstxt)
  - [1.2. How does the app working ?](#12-how-does-the-app-working-)
    - [1.2.1. Endpoints](#121-endpoints)


# 1. Language Services aka LaSer
> This project is ment to provide basic operations over language over API

## 1.1. Instalation

> For instalation there is file `requirements.txt` in which pip dependencies are stored and Dockerfile that can be ran to create container with running app.

### 1.1.1. Docker

> Docker is used for running containers, more [here](https://docs.docker.com/)


#### 1.1.1.1. Build docker image
 - `docker build -t laser:latest .`

#### 1.1.1.2. Create running container 
 - `docker run -it --rm --name laser`
<br/><br/>

---

### 1.1.2. Manual setup
 > Disclaimer: na firemní síti může být problém s proxy a SSL

#### 1.1.2.1. Install python
 - download and install python from [link](https://www.python.org/downloads/)
<br/><br/>

#### 1.1.2.2. Install venv module
 - `pip install virtualenv` 
 - `pip install --trusted-host pypi.org --trusted-host files.pythonhosted.org virtualenv`
<br/><br/>

#### 1.1.2.3. Create virtual enviroment
 - `python -m venv venv` -> this will create venv folder with python virtual enviroment
<br/><br/>

#### 1.1.2.4. Create virtual enviroment
 - Linux: `source venv/bin/activate`
 - Windows PowerShell: `.\venv\Scripts\Activate.ps1`
 - Windows cmd: `.\venv\Scripts\activate.bat`<br/><br/>

#### 1.1.2.5. Install install requirements.txt
 - `pip install -r requirements.txt`
 - `pip install -r --trusted-host pypi.org --trusted-host files.pythonhosted.org requirements.txt`<br/><br/>

## 1.2. How does the app working ?

> This application in served as microservice
> Main purpouse of the application is to lematizate text over API interface

### 1.2.1. Endpoints

 - Root - /
   > root of the api is serving documentation
 - API - /api/v01/process_data/
   > This endpoint is accepting application/json data in following format:

      ```json
      {
        "data": list[str],
        "language": str
      }
      ```

    > Example
  
      ```json
      {
        "data": ["List of string, each string is processed independently", "String No.2"],
        "language": "en"
      }
      ```
    > The api is returning application/json in following format: `list[str]`
  
      ```json
        [
          "list of string, each string be processe independently",
          "St No.2"
        ]
      ```
  