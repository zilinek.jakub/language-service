FROM harbor.k8ss.cpas.cz/docker-hub/library/python:3.9

# enviroment variables
ENV http_proxy  "http://fwdc2.cpas.cz:3128"
ENV https_proxy "http://fwdc2.cpas.cz:3128"
ENV no_proxy    "localhost,127.0.0.1,.cpas.cz,.gpcz.corp.local"

WORKDIR /app

# install dependencies
COPY requirements.txt .
RUN pip config --user set global.index-url http://nexus.cpas.cz/repository/python-group/simple && \
    pip config --user set global.trusted-host nexus.cpas.cz && \
    python -m pip install --upgrade pip && \
    pip install -r requirements.txt

COPY . .

# certificate for urllib.py
COPY ca.pem /etc/ssl/certs/ca.pem
RUN  update-ca-certificates --fresh

CMD [ "python", "app.py" ]
