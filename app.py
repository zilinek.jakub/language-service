from flask import Flask
from flask_restful import Api

from src.views import view, Lematization
from src.setup import create_docs, init_lemmatization, fix_ssl
from src.setup import create_docs, init_lemmatization

# Default flask
app = Flask(__name__, template_folder='./templates/', static_folder='./static/')
app.register_blueprint(view)

with app.app_context():
    fix_ssl()
    create_docs()
    init_lemmatization()

# Flask restful
api = Api(app)
api.add_resource(Lematization ,"/api/v01/process_data/")


if __name__ == "__main__":
    app.logger.info("The app is ready to run...\n\n\n\n")
    app.run(debug=True)